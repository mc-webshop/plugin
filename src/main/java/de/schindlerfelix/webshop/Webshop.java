package de.schindlerfelix.webshop;

import de.schindlerfelix.webshop.commands.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

public final class Webshop extends JavaPlugin {
    /**
     * Gets initialized when the Plugin is enabled
     */
    public static @NotNull Logger logger;

    /**
     * Basically the constructor
     */
    @Override
    public void onEnable() {
        // Plugin startup logic
        logger = getLogger();

        try {
            getCommand("webshop").setExecutor(new WSCommand());
            getCommand("register").setExecutor(new RegisterCommand());
            getCommand("namechange").setExecutor(new NameChangeCommand());
            getCommand("balance").setExecutor(new BalanceCommand());
            logger.info("Registered webshop commands");
        } catch (NullPointerException ignored) {
            logger.info("Failed to register webshop commands");
        }

        getServer().getPluginManager().registerEvents(new WSCloseListener(), this);
    }

    /**
     * Basically the destructor
     */
    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
