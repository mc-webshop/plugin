package de.schindlerfelix.webshop.data;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.json.JSONArray;
import org.json.JSONObject;

public class InventoryModel {
    /**
    *
    * @param uuid Player UUID
    * @return The webshop inventory of the player
    * @throws UnirestException When error during load occurs
    */
    public static Inventory loadInventory(String uuid) throws UnirestException {
        final var res = API.GET("/api/inv?uuid=" + uuid);
        final var itemStacks = new JSONArray(res);

        Webshop.logger.info("Inventory of " + uuid + ": " + itemStacks);

        final var inv = Bukkit.createInventory(null, InventoryType.CHEST, Component.text("Webshop Inventory"));

        if (!itemStacks.isEmpty()) {
            for (int i = 0; i < itemStacks.length(); i++) {
                final var stack = itemStacks.getJSONObject(i);

                try {
                    final var c = new YamlConfiguration();
                    c.loadFromString(stack.getString("itemMeta"));
                    final var itemStack = c.getItemStack("is");

                    if (itemStack != null) {
                        inv.addItem(itemStack);
                    } else {
                        Webshop.logger.warning("Failed to get item from config");
                    }
                } catch (InvalidConfigurationException e) {
                    Webshop.logger.warning("Failed to deserialize item");
                    Webshop.logger.warning(e.getLocalizedMessage());
                }
            }
        }

        return inv;
    }

    /**
    *
    * @param uuid Player UUID
    * @param inv Inventory to be saved
    * @throws UnirestException When error during save occurs
    */
    public static boolean saveInventory(String uuid, Inventory inv) throws UnirestException {
        var data = new JSONArray();
        var itemCount = 0;

        for (int i = 0; i < InventoryType.CHEST.getDefaultSize(); i++) {
            final var item = inv.getItem(i);

            if (item != null) {
                final var c = new YamlConfiguration();
                c.set("is", item);
                final var itemMeta = c.saveToString();

                Webshop.logger.info("Item Meta: " + itemMeta);

                final var jsonItem = new JSONObject();
                jsonItem.put("item", item.getType());
                jsonItem.put("quantity", item.getAmount());
                jsonItem.put("meta", itemMeta);

                data = data.put(jsonItem);
                itemCount++;
            }
        }

        Webshop.logger.info("Saving inventory of " + uuid + ": " + data);
        var apiRes = API.POST("/api/inv?uuid=" + uuid, data.toString());

        var insertedCount = new JSONObject(apiRes).getInt("inserted");
        return insertedCount == itemCount;
    }
}
