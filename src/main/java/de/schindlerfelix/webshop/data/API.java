package de.schindlerfelix.webshop.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class API {
    /**
     * URI of PocketBase instance
     */
    private static final String base = "https://mc.schindlerfelix.de";

    /**
     * @param endpoint API endpoint
     * @return API response as String
     * @throws UnirestException When anything is wrong
     */
    public static String GET(String endpoint) throws UnirestException {
        HttpResponse<String> httpResponse = Unirest.get(base + endpoint).asString();
        return httpResponse.getBody();
    }

    /**
     * @param endpoint API endpoint
     * @param data     JSONArray as String
     * @return API response as String
     * @throws UnirestException When anything is wrong
     */
    public static String POST(String endpoint, String data) throws UnirestException {
        HttpResponse<String> httpResponse = Unirest.post(base + endpoint)
                .header("Content-Type", "application/json")
                .body(data)
                .asString();
        return httpResponse.getBody();
    }
}
