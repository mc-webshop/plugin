package de.schindlerfelix.webshop.data;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class UserModel {
    /**
     * Registers a user for the webshop
     *
     * @param uuid Player UUID
     * @param username Player username
     * @param password (New) password to be set
     */
    public static void register(String uuid, String username, String password) throws UnirestException {
        final var data = new JSONObject();
        data.put("uuid", uuid);
        data.put("username", username);
        data.put("password", password);

        API.POST("/api/reg", data.toString());
    }

    /**
     * Changes the nickname of a user in the webshop (used for login, ...)
     *
     * @param uuid Player UUID
     * @param username Player username
     */
    public static void changeName(String uuid, String username) throws UnirestException {
        final var data = new JSONObject();
        data.put("uuid", uuid);
        data.put("username", username);

        API.POST("/api/namechange", data.toString());
    }

    /**
     * Get's the balance of a user
     *
     * @param playerName Player username
     */
    public static String balance(String playerName) throws UnirestException {
        return API.GET("/api/balance?username=" + playerName);
    }
}
