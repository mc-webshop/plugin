package de.schindlerfelix.webshop.commands;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import de.schindlerfelix.webshop.data.UserModel;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class RegisterCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof final Player player) {
            if (args.length > 0) {
                final var pass = args[0];
                try {
                    UserModel.register(player.getUniqueId().toString(), player.getName(), pass);
                    player.sendMessage(ChatColor.GREEN + "You can now log in!");

                    return true;
                } catch (UnirestException ignored) {
                    player.sendMessage(ChatColor.RED + "Failed to register, if you're not already registered please report this bug on Discord and try again later");
                    Webshop.logger.warning("Failed to register user " + player.getUniqueId());
                }
            } else {
                player.sendMessage(ChatColor.RED + "You have to provide a password");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
        }

        return false;
    }
}
