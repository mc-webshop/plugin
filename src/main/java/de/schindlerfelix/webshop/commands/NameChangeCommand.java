package de.schindlerfelix.webshop.commands;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import de.schindlerfelix.webshop.data.UserModel;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class NameChangeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof final Player player) {
            final var uuid = player.getUniqueId().toString();
            final var playerName = player.getName();

            try {
                UserModel.changeName(uuid, playerName);
                player.sendMessage("You can now log in with your new username " + playerName);
            } catch (UnirestException ignored) {
                player.sendMessage(ChatColor.RED + "Failed to change name, please report this bug on Discord and try again later");
                Webshop.logger.warning("Failed to change name of " + uuid);
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
        }

        return false;
    }
}
