package de.schindlerfelix.webshop.commands;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import de.schindlerfelix.webshop.data.UserModel;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class BalanceCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String playerName = null;

        if (args.length > 0) {
            playerName = args[0];
        } else if (sender instanceof final Player player) {
            playerName = player.getName();
        } else {
            sender.sendMessage(ChatColor.RED + "You need to specify a player name or be one yourself");
            return false;
        }

        try {
            final var balance = UserModel.balance(playerName);
            sender.sendMessage("Balance of " + playerName + " is " + balance);

            return true;
        } catch (UnirestException ignored) {
            sender.sendMessage(ChatColor.RED + "Failed to get balance, please report this bug on Discord and try again later");
            Webshop.logger.warning("Failed to get balance of user " + playerName);
        }

        return false;
    }
}
