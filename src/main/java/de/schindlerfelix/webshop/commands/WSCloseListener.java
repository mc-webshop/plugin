package de.schindlerfelix.webshop.commands;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import de.schindlerfelix.webshop.data.InventoryModel;
import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class WSCloseListener implements Listener {
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        final var title = e.getView().title();

        if (title.equals(Component.text("Webshop Inventory"))) {
            final var player = e.getPlayer();
            var success = false;

            try {
                success = InventoryModel.saveInventory(player.getUniqueId().toString(), e.getInventory());
            } catch (UnirestException ignored) {
                Webshop.logger.info("API request for inventory of " + player.getUniqueId() + " failed");
            }

            if (success)
                player.sendMessage("Saved webshop inventory");
            else {
                player.sendMessage(ChatColor.RED + "Failed to save webshop inventory");
                Webshop.logger.warning("Failed to save inv via API of player " + player.getUniqueId());
            }
        }
    }
}
