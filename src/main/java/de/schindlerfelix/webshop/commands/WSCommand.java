package de.schindlerfelix.webshop.commands;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.schindlerfelix.webshop.Webshop;
import de.schindlerfelix.webshop.data.InventoryModel;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class WSCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof final Player player) {
            try {
                player.openInventory(InventoryModel.loadInventory(player.getUniqueId().toString()));
                return true;
            } catch (UnirestException e) {
                sender.sendMessage(ChatColor.RED + "Failed to get your inventory");
                Webshop.logger.warning("Failed to load inv via API of player " + player.getUniqueId());
            }
        }

        return false;
    }
}
